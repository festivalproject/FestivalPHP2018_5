<?php

namespace modele\dao;
use modele\metier\Representation;
use modele\metier\Lieu;
use modele\metier\Groupe;
use PDOStatement;
use PDO;

/**
 * Description of RepresentationDao
 *
 * @author btssio
 */
class RepresentationDao {

    /**
     * Instancier un objet de type Representation à partir d'un enregistrement de la table REPRESENTATION
     * @param array $enreg
     * @return Representation
     */
    protected static function enregVersMetier(array $enreg) {
        $id = $enreg['ID'];
        $groupe = GroupeDAO::getOneById($enreg['IDGROUPE']);
        $lieu = LieuDAO::getOneById($enreg['IDLIEU']);
        $date = $enreg['DATE'];
        $heureDebut = $enreg['HEUREDEBUT'];
        $heureFin = $enreg['HEUREFIN'];
        $uneRepresentation = new Representation($id, $groupe, $lieu, $date, $heureDebut, $heureFin);

        return $uneRepresentation;
    }

    /**
     * Valorise les paramètres d'une requête préparée avec l'état d'un objet Representation
     * @param Representatin $objetMetier une Representatin
     * @param PDOStatement $stmt requête préparée
     */
    protected static function metierVersEnreg(Representation $objetMetier, PDOStatement $stmt) {
        $stmt->bindValue(':ID', $objetMetier->getId());
        $stmt->bindValue(':IDGROUPE', $objetMetier->getGroupe()->getId());
        $stmt->bindValue(':IDLIEU', $objetMetier->getLieu()->getId());
        $stmt->bindValue(':DATE', $objetMetier->getDate());
        $stmt->bindValue(':HEUREDEBUT', $objetMetier->getHeureDebut());
        $stmt->bindValue(':HEUREFIN', $objetMetier->getHeureFin());
    }

    /**
     * Retourne la liste de toutes les Representation
     * @return array tableau d'objets de type Representation
     */
    public static function getAll() {
        $lesObjets = array();
        $requete = "SELECT * FROM Representation ORDER BY ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        $ok = $stmt->execute();
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Recherche une representation selon la valeur de son identifiant
     * @param string $id
     * @return Representation la representation trouvé ; null sinon
     */
    public static function getOneById($id) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok && $stmt->rowCount() > 0) {
            $objetConstruit = self::enregVersMetier($stmt->fetch(PDO::FETCH_ASSOC));
        }
        return $objetConstruit;
    }

    /**
     * Retourne toutes les représentations triées par date
     * @return type
     */
    public static function getRepresentationByDate($date) {
        $objetConstruit = null;
        $requete = "SELECT * FROM Representation WHERE DATE=:DATE";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':DATE', $date);
        $ok = $stmt->execute();
        // attention, $ok = true pour un select ne retournant aucune ligne
        if ($ok) {
            while ($enreg = $stmt->fetch(PDO::FETCH_ASSOC)) {
                $lesObjets[] = self::enregVersMetier($enreg);
            }
        }
        return $lesObjets;
    }

    /**
     * Insérer un nouvel enregistrement dans la table à partir de l'état d'un objet métier
     * @param Representation $objet objet métier à insérer
     * @return boolean =FALSE si l'opération échoue
     */
    public static function insert(Representation $objet) {
        $requete = "INSERT INTO Representation VALUES (:ID, :IDGROUPE, :IDLIEU, :DATE, :HEUREDEBUT, :HEUREFIN)";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        if(!RepresentationDao::isAnExistingId($objet->getId())){
           $ok = $stmt->execute();
            return ($ok && $stmt->rowCount() > 0);
        }else{
            $ok = false;
        }
    }

    /**
     * Mettre à jour enregistrement dans la table à partir de l'état d'un objet métier
     * @param string identifiant de l'enregistrement à mettre à jour
     * @param Representation $objet objet métier à mettre à jour
     * @return boolean =FALSE si l'opérationn échoue
     */
    public static function update($id, Representation $objet) {
        $ok = false;
        $requete = "UPDATE Representation SET IDGROUPE=:IDGROUPE, IDLIEU=:IDLIEU, DATE=:DATE, HEUREDEBUT=:HEUREDEBUT,
            HEUREFIN=:HEUREFIN WHERE ID=:ID";
        $stmt = Bdd::getPdo()->prepare($requete);
        self::metierVersEnreg($objet, $stmt);
        $stmt->bindParam(':ID', $id);
        $ok = $stmt->execute();
        return ($ok && $stmt->rowCount() > 0);
    }

    /**
     * Détruire un enregistrement de la table REPRESENTATION d'après son identifiant
     * @param string identifiant de l'enregistrement à détruire
     * @return boolean =TRUE si l'enregistrement est détruit, =FALSE si l'opération échoue
     */
    public static function delete($id) {
        $ok = false;
        $requete = "DELETE FROM Representation WHERE ID = :id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $ok = $stmt->execute();
        $ok = $ok && ($stmt->rowCount() > 0);
        return $ok;
    }

    /**
     * Permet de vérifier s'il existe ou non une Representation ayant déjà le même identifiant dans la BD
     * @param string $id identifiant de la Representation à tester
     * @return boolean =true si l'id existe déjà, =false sinon
     */
    public static function isAnExistingId($id) {
        $requete = "SELECT COUNT(*) FROM Representation WHERE ID=:id";
        $stmt = Bdd::getPdo()->prepare($requete);
        $stmt->bindParam(':id', $id);
        $stmt->execute();
        return $stmt->fetchColumn(0);
    }

}
