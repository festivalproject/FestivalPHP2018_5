<?php
namespace modele\metier;

/**
 * Description of Representation
 * @author btssio
 */
class Representation {
    /**
     * identifiant de la Representation ("xxxxR")
     * @var String
     */
    private $id;
    /**
     * identifiant du groupe
     * @var Groupe
     */
    private $groupe;
    /**
     * identifiant du lieu
     * @var Lieu
     */
    private $lieu;
    /**
     * date de la représentation
     * @var date
     */
    private $date;
    /**
     * heure de début de la représentation
     * @var Integer
     */
    private $heureDebut;
    /**
     * heure de fin de la représentation
     * @var Integer
     */
    private $heureFin;

    function __construct($id, Groupe $groupe, Lieu $lieu, $date, $heureDebut, $heureFin) {
        $this->id = $id;
        $this->groupe = $groupe;
        $this->lieu = $lieu;
        $this->date = $date;
        $this->heureDebut = $heureDebut;
        $this->heureFin = $heureFin;
    }

    function getId() {
        return $this->id;
    }

    function getGroupe() {
        return $this->groupe;
    }

    function getLieu() {
        return $this->lieu;
    }

    function getDate() {
        return $this->date;
    }

    function getHeureDebut() {
        return $this->heureDebut;
    }

    function getHeureFin() {
        return $this->heureFin;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setGroupe(Groupe $groupe) {
        $this->groupe = $groupe;
    }

    function setLieu(Lieu $lieu) {
        $this->lieu = $lieu;
    }

    function setDate($date) {
        $this->date = $date;
    }

    function setHeureDebut($heureDebut) {
        $this->heureDebut = $heureDebut;
    }

    function setHeureFin($heureFin) {
        $this->heureFin = $heureFin;
    }
}
