<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Representation Test</title>
    </head>
    <body>
        <?php
        use modele\metier\Representation;
        use modele\metier\Groupe;
        use modele\metier\Lieu;
        require_once __DIR__ . '/../../includes/autoload.inc.php';
        echo "<h2>Test unitaire de la classe métier Representation</h2>";
        $unGroupe = new Groupe("g999","les Joyeux Turlurons","général Alcazar","Tapiocapolis" ,25,"San Theodoros","N");
        $unLieu = new Lieu('0589L','Le Calamar','45 rue du poulet','35407' ,'Saint-Malo',300);
        $objet = new Representation('b9999', $unGroupe, $unLieu, '2019-01-15', '20:00', '21:45');
        var_dump($objet);
        ?>
    </body>
</html>